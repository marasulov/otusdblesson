﻿using System;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace OtusDBLesson
{
    /*
     * Домашнее задание
       
       Подключаем базы данных к проекту
       Цель:
       
       В этом домашнем задании вы научитесь создавать базу данных с таблицами, а также писать скрипты наполнения таблиц данными. Но самое главное - вы создадите приложение, способное получать данные из базы и добавлять новые.
       
       Создать базу данных PostgreSQL для одной из компаний на выбор: Авито, СберБанк, Otus или eBay. Написать скрипт создания 3 таблиц, которые должны иметь первичные ключи и быть соединены внешними ключами.
       Написать скрипт заполнения таблиц данными, минимум по пять строк в каждую.
       Создать консольную программу, которая выводит содержимое всех таблиц.
       Добавить в программу возможность добавления в таблицу на выбор.
       
     */
    class Program
    {
        static void Main(string[] args)
        {

            string sqlQueryCreateStudenstTable = @"CREATE SEQUENCE IF NOT EXISTS students_id_seq;
            CREATE TABLE students
            (
                id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('students_id_seq'),
                first_name      CHARACTER VARYING(255)      NOT NULL,
                last_name       CHARACTER VARYING(255)      NOT NULL,
                email           CHARACTER VARYING(255)      NOT NULL,
              
                CONSTRAINT students_pkey PRIMARY KEY (id),
                CONSTRAINT students_email_unique UNIQUE (email)
            );

            CREATE INDEX students_last_name_idx ON students(first_name);
            CREATE UNIQUE INDEX students_email_unq_idx ON students(lower(email));
            ";

            string sqlQueryCreateCoursesTable = @"CREATE SEQUENCE IF NOT EXISTS courses_id_seq;

                CREATE TABLE courses
                (
                    id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('courses_id_seq'),
                    course_name     CHARACTER VARYING(255)      NOT NULL,
                    status          CHARACTER VARYING(255)      NOT NULL,
                    start_day       TIMESTAMP WITH TIME ZONE    NOT NULL,
                    finish_day      TIMESTAMP WITH TIME ZONE    NOT NULL,
                  
                    CONSTRAINT courses_pkey PRIMARY KEY (id)
                );
                ";

            string sqlQueryCreateStudentsCoursesTable = @"CREATE SEQUENCE IF NOT EXISTS students_courses_id_seq;
                
                CREATE TABLE students_courses
                (
                    id              BIGINT                       NOT NULL DEFAULT NEXTVAL('students_courses_id_seq'),
	                student_id      BIGINT                       NOT NULL,
	                courses_id      BIGINT                       NOT NULL,
	                
                    CONSTRAINT courses_students_pkey PRIMARY KEY (id),

                    CONSTRAINT student_courses_fk_student_id FOREIGN KEY (student_id) REFERENCES students(id) ON DELETE CASCADE,
                    CONSTRAINT student_courses_fk_courses_id FOREIGN KEY (courses_id) REFERENCES courses(id) ON DELETE CASCADE
                )";

            var sqlInsertIntoStudents1 = @"
            INSERT INTO students(first_name, last_name, email) 
            VALUES ('Имя1', 'Фамилия1', 'imya1@mail.ru');";
            var sqlInsertIntoStudents2 = @"
            INSERT INTO students(first_name, last_name, email) 
            VALUES ('Имя2', 'Фамилия2', 'imya2@mail.ru');";
            var sqlInsertIntoStudents3 = @"
            INSERT INTO students(first_name, last_name, email) 
            VALUES ('Имя1', 'Фамилия', 'imya3@mail.ru');";


            var sqlInsertIntoCourses1 = @"
            INSERT INTO courses(course_name, status, start_day, finish_day) 
            VALUES ('C# developer', 'started', '2021-01-01 00:00:01','2021-06-01 00:00:01');";
            var sqlInsertIntoCourses2 = @"
            INSERT INTO courses(course_name, status, start_day, finish_day) 
            VALUES ('Java developer ', 'not start', '2021-02-01 00:00:01','2021-08-01 00:00:01');";
            var sqlInsertIntoCourses3 = @"
            INSERT INTO courses(course_name, status, start_day, finish_day) 
            VALUES ('MSSQL developer ', 'finished', '2021-03-01 00:00:01','2021-07-01 00:00:01');";


            var sqlInsertIntoStudentCourses1 = @"INSERT INTO students_courses(student_id, courses_id) values (1, 1)";
            var sqlInsertIntoStudentCourses2 = @"INSERT INTO students_courses(student_id, courses_id) values (2, 3)";
            var sqlInsertIntoStudentCourses3 = @"INSERT INTO students_courses(student_id, courses_id) values (1, 2)";
            var sqlInsertIntoStudentCourses4 = @"INSERT INTO students_courses(student_id, courses_id) values (3, 1)";

            ConnectionDB connectionDb = new ConnectionDB();
            connectionDb.GetConnection();

            var getStudentsCources = @"
            SELECT s.first_name, s.last_name, c.course_name
            FROM students_courses AS sc INNER JOIN
            courses AS c ON c.id = sc.courses_id LEFT JOIN
            students AS s ON s.Id = sc.student_id";

            var getActiveCourses = @"
            SELECT course_name, start_day, finish_day
            FROM courses WHERE CURRENT_DATE
            BETWEEN start_day and finish_day
            ";

            var getStudentFinishedCourses = @"
            SELECT s.first_name, s.last_name, c.course_name, c.finish_day
            FROM students_courses AS sc INNER JOIN
            courses AS c ON c.id = sc.courses_id LEFT JOIN
            students AS s ON s.Id = sc.student_id
			WHERE CURRENT_DATE >c.finish_day
            ";

            Console.WriteLine("Список студентов учашихся на курсах");
            connectionDb.GetSelectQuery(getStudentsCources);
            
            Console.WriteLine("\nСписок активных курсов");
            connectionDb.GetSelectQuery(getActiveCourses);

            Console.WriteLine("\nСписок студентов которые закончили курс");
            connectionDb.GetSelectQuery(getStudentFinishedCourses);

            using (TestdbContext db = new TestdbContext())
            {
                var courses = db.Courses.Include(a => a.Lessons).ToList();

                for (int i = 0; i < courses.Count; i++)
                {
                    Console.WriteLine(courses[i].CourseName);
                    foreach (var lesson in courses[i].Lessons)
                    {
                        Console.WriteLine($"         {lesson}");
                    }
                }
            }

            Console.WriteLine("Enter student name");
            string name = Console.ReadLine();

            Console.WriteLine("Enter student lastname");
            string lastname = Console.ReadLine();

            Console.WriteLine("Enter student email");
            string email = Console.ReadLine();

            using (var db = new TestdbContext() )
            {
                var student = new Student()
                {
                    FirstName = name,
                    LastName = lastname,
                    Email = email
                };

                db.Students.Add(student);
                db.SaveChanges();

                Console.WriteLine(student.FirstName);
            }


        }
    }

}
