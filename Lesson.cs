﻿using System;
using System.Collections.Generic;

#nullable disable

namespace OtusDBLesson
{
    public partial class Lesson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public long CourseId { get; set; }

        public virtual Course Course { get; set; }
    }
}
