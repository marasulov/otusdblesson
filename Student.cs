﻿using System;
using System.Collections.Generic;

#nullable disable

namespace OtusDBLesson
{
    public partial class Student
    {
        public Student()
        {
            StudentsCourses = new HashSet<StudentsCourse>();
        }

        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public virtual ICollection<StudentsCourse> StudentsCourses { get; set; }
    }
}
