﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

using System.IO;


#nullable disable

namespace OtusDBLesson
{
    public partial class TestdbContext : DbContext
    {
        private IConfiguration _configuration;

        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Lesson> Lessons { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<StudentsCourse> StudentsCourses { get; set; }


        public TestdbContext()
        {

        }

        public TestdbContext(DbContextOptions<TestdbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            var configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            _configuration = configurationBuilder.Build();

            optionsBuilder.UseNpgsql(_configuration.GetConnectionString("default"));

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Russian_Russia.1251");

            modelBuilder.Entity<Course>(entity =>
            {
                entity.ToTable("courses");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CourseName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("course_name");

                entity.Property(e => e.FinishDay)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("finish_day");

                entity.Property(e => e.StartDay)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("start_day");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("status");
            });

            modelBuilder.Entity<Lesson>(entity =>
            {
                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Lessons)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("lessons_fk_cources_id");
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.ToTable("students");

                entity.HasIndex(e => e.Email, "students_email_unique")
                    .IsUnique();

                entity.HasIndex(e => e.FirstName, "students_last_name_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("first_name");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("last_name");
            });

            modelBuilder.Entity<StudentsCourse>(entity =>
            {
                entity.ToTable("students_courses");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CoursesId).HasColumnName("courses_id");

                entity.Property(e => e.StudentId).HasColumnName("student_id");

                entity.HasOne(d => d.Courses)
                    .WithMany(p => p.StudentsCourses)
                    .HasForeignKey(d => d.CoursesId)
                    .HasConstraintName("student_courses_fk_courses_id");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentsCourses)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("student_courses_fk_student_id");
            });

            modelBuilder.HasSequence("courses_id_seq");

            modelBuilder.HasSequence("students_courses_id_seq");

            modelBuilder.HasSequence("students_id_seq");

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
