﻿using System;
using System.Collections.Generic;

#nullable disable

namespace OtusDBLesson
{
    public partial class Course
    {
        public Course()
        {
            Lessons = new HashSet<Lesson>();
            StudentsCourses = new HashSet<StudentsCourse>();
        }

        public long Id { get; set; }
        public string CourseName { get; set; }
        public string Status { get; set; }
        public DateTime StartDay { get; set; }
        public DateTime FinishDay { get; set; }

        public virtual ICollection<Lesson> Lessons { get; set; }
        public virtual ICollection<StudentsCourse> StudentsCourses { get; set; }
    }
}
