﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;


namespace OtusDBLesson
{
    public class ConnectionDB:IDisposable
    {
        const string connectionStr = "Host=localhost;Username=postgres;Password=admin;Database=testdb";
        private NpgsqlConnection _connection;
        public ConnectionDB()
        {
           _connection = new NpgsqlConnection(connectionStr);
        }


        public void GetConnection()
        {
            _connection.Open();
        }

        public void GetQuery(string sql)
        {
            using var cmd = new NpgsqlCommand(sql, _connection);

            var affectedRows = cmd.ExecuteNonQuery().ToString();
            Console.WriteLine($"Запрос {sql} выполнен");
        }

        public void GetSelectQuery(string sql)
        {
            using var cmd = new NpgsqlCommand(sql, _connection);

            string val="";
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    Console.Write(reader[i].ToString().Trim());
                    Console.Write("|");
                }

                Console.WriteLine();
            }
            reader.Close();

        }

        public void Dispose()
        {
            _connection.Dispose();
        }
    }
}
