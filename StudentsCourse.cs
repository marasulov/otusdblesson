﻿using System;
using System.Collections.Generic;

#nullable disable

namespace OtusDBLesson
{
    public partial class StudentsCourse
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public long CoursesId { get; set; }

        public virtual Course Courses { get; set; }
        public virtual Student Student { get; set; }
    }
}
